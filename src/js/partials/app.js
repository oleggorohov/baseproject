/*MAPS*/

var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);

function init() {
    // Создание экземпляра карты и его привязка к контейнеру с
    // заданным id ("map").
    myMap = new ymaps.Map('map', {
        // При инициализации карты обязательно нужно указать
        // её центр и коэффициент масштабирования.
        center: [55.754347, 37.617646],
        zoom: 11,
        // controls: []
    })
        ,
        myPlacemark = new ymaps.Placemark([55.664639, 37.259953]);
    myMap.behaviors.disable('scrollZoom');
    myMap.geoObjects.add(myPlacemark);
    // myMap.balloon.open([55.754347, 37.617646], '"', {
    //     // Опция: не показываем кнопку закрытия.
    //     closeButton: true
    // });
};
$(function(){

    //отправляем форму по клику на кнопку
    $(document).on('click', '.js-submit', function(e){
        e.preventDefault();
        $(this).siblings('.form__submit').trigger('click');
    });
    $('#form').on('submit', function(e){
        e.preventDefault();
        call();
    });
    $('#modal-form').on('submit', function(e){
        e.preventDefault();
        callModal();
    });
    // плавная прокрутка к элементу
    $(document).on('click', '.js-anchor', function(){
        var el = $(this);
        var dest = el.attr('href'); // получаем направление
        if(dest !== undefined && dest !== '') { // проверяем существование
            $('html').animate({
                    scrollTop: $(dest).offset().top // прокручиваем страницу к требуемому элементу
                }, 500 // скорость прокрутки
            );
        }
        return false;
    });

$('.reviews').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    arrows: true,
    // appendArrows: $('.reviews-buttons'),
    prevArrow: '<div class="arrow arrow--prev"><img src="img/left-arrow.png" alt=""></div>',
    nextArrow: '<div class="arrow arrow--next"><img src="img/right-arrow.png" alt=""></div>'
});
});
function callModal() {
    var msg   = $('#modal .form').serialize();
    $.ajax({
        type: 'POST',
        url: 'mail.php',
        data: msg,
        success: function(data) {
            $('#modal .form').hide();
            $('#modal .form__title').hide();
            $('#modal .success').addClass('active');
        },
        error:  function(xhr, str){
            console.log('Возникла ошибка: ' + xhr.responseCode);
        }
    });

}
function call() {
    var msg   = $('#claim .form').serialize();
    $.ajax({
        type: 'POST',
        url: 'mail.php',
        data: msg,
        success: function(data) {
            $('#claim .form').hide();
            $('#claim .form__title').hide();
            $('#claim .success').addClass('active');
        },
        error:  function(xhr, str){
            console.log('Возникла ошибка: ' + xhr.responseCode);
        }
    });

}